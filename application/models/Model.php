<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent 
{
	protected $table = 'model';
    public $timestamps = false;
}