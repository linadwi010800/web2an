<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* ====================================================================== */

if (! function_exists('getNow')) {
    
    function getNow()
    {
        return Date('d') . '-' . Date('m') . '-' . Date('Y');
    }

}

if (! function_exists('getNowReverse')) {
    
    function getNowReverse()
    {
        return Date('Y') . '-' . Date('m') . '-' . Date('d');
    }

}

if (! function_exists('checkItem')) {
    
    function checkItem($item)
    {
        if ($item == null | $item == 0) {
            return 'display-none';
        }
    }

}

if (! function_exists('hitung_umur')) {

    function hitung_umur($date)
    {
        $bday = new Datetime($date);
        $today = new Datetime();

        $diff = $today->diff($bday);

        if ($diff->y < 1) {

            if ($diff->m < 1) {
                return $diff->d. ' Hari';
            }

            return $diff->m. ' Bulan';
        }

        return $diff->y. ' Tahun';
    }

}

if (! function_exists('nama_simpel')) {
    
    // $kata is array
    function nama_simpel($kata, $nama)
    {
        $simple = ucwords(strtolower($nama));

        for ($i = 0; $i < count($kata); $i++) { 
            $simple = str_replace(ucwords(strtolower($kata[$i])), "", $simple);
        }

        return $simple;
    }

}

if (! function_exists('namaAdmin')) {

    function namaAdmin($kata)
    {
        return str_replace("Admin", "Unit Pelayanan Rekamin", ucwords(strtolower($kata)));
    }

}

if (! function_exists('jenis_kelamin')) {

    function jenis_kelamin($jk)
    {
        if ($jk == 'l') {

            return 'Laki - Laki';
        }

        return 'Perempuan';
    }

}

if (! function_exists('aktif')) {

    function aktif($aktif)
    {
        if ($aktif == 'y') {

            return 'Aktif';
        }

        return 'Tidak Aktif';
    }

}

if (! function_exists('kasta')) {

    function kasta($kasta)
    {
        if ($kasta == 'superuser') {

            return 'Administrator';

        } else if ($kasta == 'dokter_umum') {

            return 'Dokter Umum';

        } else if ($kasta == 'dokter_gigi') {

            return 'Dokter Gigi';
            
        } else if ($kasta == 'apoteker') {

            return 'Apoteker';
            
        }

        return 'Perawat';
    }

}

if (! function_exists('uang_rp')) {

    function uang_rp($angka)
    {
        return "<span class='label label-mega label-success label-block'>Rp ".number_format($angka, 0, ',', '.')."</span>";
    }

}

/* ====================== General Helper ================================ */

if (! function_exists('strtogeneral')) {
    
    function strtogeneral($str)
    {
        return ucwords(str_replace('_', ' ', $str));
    }

}

if (! function_exists('getRp')) {

    function getRp($angka)
    {
        return "Rp ".number_format($angka, 0, ',', '.').",-";
    }

}

if (! function_exists('json_flash')) {

    function json_flash($name, $data)
    {
        return json_decode(get_flash($name))->$data;
    }

}

if (! function_exists('json_data')) {
    
    function json_data($name)
    {
        return json_decode(get_session($name));
    }

}

if ( ! function_exists('terbilang')) {
    
    function terbilang($angka)
    {
        return ltrim(ucwords(strtolower(toWords($angka)) . ' Rupiah'));
    }

}

if (! function_exists('toWords')) {

    function toWords($satuan)
    {
        $huruf = [
            '', 'SATU', 'DUA', 'TIGA', 'EMPAT', 'LIMA', 'ENAM',
            'TUJUH', 'DELAPAN', 'SEMBILAN', 'SEPULUH', 'SEBELAS'
        ];

        if ($satuan < 12)
            return " ".$huruf[$satuan];

        elseif ($satuan < 20)
            return toWords($satuan - 10)." BELAS";

        elseif ($satuan < 100)
            return toWords($satuan / 10)." PULUH".toWords($satuan % 10);

        elseif ($satuan < 200)
            return "SERATUS".toWords($satuan - 100);

        elseif ($satuan < 1000)
            return toWords($satuan / 100)." RATUS".toWords($satuan % 100);

        elseif ($satuan < 2000)
            return "SERIBU".toWords($satuan - 1000);

        elseif ($satuan < 1000000)
            return toWords($satuan / 1000)." RIBU".toWords($satuan % 1000);

        elseif ($satuan < 1000000000)
            return toWords($satuan / 1000000)." JUTA".toWords($satuan % 1000000);

        elseif ($satuan >= 1000000000)
            echo "Angka terlalu Besar";

    }

}

if (! function_exists('uniq_id')) {
    
    function uniq_id($string, $length, $id)
    {
        return $string.sprintf('%0'.$length.'d', $id);
    }

}

if (! function_exists('tgl_indo')) {

    function tgl_indo($tanggal)
    {
        $year = substr($tanggal, 0, 4);
        $month = substr($tanggal, 5, 2);
        $date = substr($tanggal, 8, 2);

        $bulan = [
          'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
          'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
        ];

        return $date . ' ' . $bulan[$month - 1] . ' ' . $year;
    }

}

if (! function_exists('tgl_waktu_indo')) {

    function tgl_waktu_indo($tanggal)
    {
        $date = tgl_indo($tanggal);
        $time = substr($tanggal, 11, 8);

        return $date . ' ' . $time;
    }

}

if ( ! function_exists('segment') )
{
    function segment($number, $string)
    {
    	$CI =& get_instance();

    	if ($CI->uri->segment($number) == $string) {
    		return 'current-menu-parent';
    	}

    	return '';
    }
}

if ( ! function_exists('get_segment') )
{
    function get_segment($number)
    {
        $CI =& get_instance();

        return $CI->uri->segment($number);
    }
}

if ( ! function_exists('section') )
{
    function section($view)
    {
    	$CI =& get_instance();

    	if ($view != null) {
    		return $CI->load->view($view, null, true);
    	}

    	return null;
    }
}

if ( ! function_exists('url') )
{
    function url($url = null)
    {
        return base_url($url);
    }
}

if ( ! function_exists('full_url')) {
    
    function full_url()
    {
        $CI =& get_instance();
        $url = $CI->config->site_url($CI->uri->uri_string());

        return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
    }

}

/* ====================== Form Helper ================================ */

if ( ! function_exists('view') )
{
    function view($name, $value = null, $bool = false)
    {
        $CI =& get_instance();

        return $CI->load->view($name, $value, $bool);
    }
}

if ( ! function_exists('post') )
{
    function post($name)
    {
        $CI =& get_instance();

        return $CI->input->post($name);
    }
}

if ( ! function_exists('get') )
{
    function get($name)
    {
        $CI =& get_instance();

        return $CI->input->get($name);
    }
}

if ( ! function_exists('validator') )
{
    function validator($name, $label = null, $rules = null)
    {
        $CI =& get_instance();

        return $CI->form_validation->set_rules($name, $label, $rules);
    }
}

if ( ! function_exists('valid') )
{
    function valid()
    {
        $CI =& get_instance();

        return $CI->form_validation->run();
    }
}

if ( ! function_exists('auth')) {
    
    function auth($name)
    {
        $CI =& get_instance();

        if ($CI->session->has_userdata($name) == null)
            return false;

        return true;
    }

}

if ( ! function_exists('session') )
{
    function session($name, $data)
    {
        $CI =& get_instance();

        return $CI->session->set_userdata($name, $data);
    }
}

if ( ! function_exists('get_session') )
{
    function get_session($name)
    {
        $CI =& get_instance();

        return $CI->session->userdata($name);
    }
}

if ( ! function_exists('destroy') )
{
    function destroy($name)
    {
        $CI =& get_instance();

        return $CI->session->unset_userdata($name);
    }
}

if ( ! function_exists('flash') )
{
    function flash($name, $data)
    {
        $CI =& get_instance();

        return $CI->session->set_flashdata($name, $data);
    }
}

if ( ! function_exists('get_flash') )
{
    function get_flash($name)
    {
        $CI =& get_instance();

        return $CI->session->flashdata($name);
    }
}

if ( ! function_exists('old') )
{
    function old($name, $value = null)
    {
        if ($value != null | $value != 0) {
            return $value;
        }

        return set_value($name);
    }
}

if ( ! function_exists('def') )
{
    function def($name, $value = null)
    {
        if ($value != null | $value != 0) {
            return $value;
        }

        return $name;
    }
}

if (! function_exists('has_error')) {

    function has_error($name)
    {
        return (form_error($name)) ? 'has-error has-feedback' : '';
    }

}

if (! function_exists('error_icon')) {

    function error_icon($name)
    {
        return (form_error($name)) ? '<div class="form-control-feedback"><i class="icon-cancel-circle2"></i></div>' : '';
    }

}

if (! function_exists('selected')) {

    function selected($name, $value)
    {
        return ($name == $value) ? 'selected' : '';
    }

}

if (! function_exists('in_explode')) {

    function in_explode($list, $value)
    {
        return (in_array($value, explode(', ', $list))) ? 'checked' : '';
    }

}

if (! function_exists('cc')) {

    function cc($param = null)
    {
        echo $param;
        // print_r($param);
        die();
    }

}

if (! function_exists('form_delete')) {

    function form_delete($url, $id)
    {
        return form_open($url, ['id' => $id]).'<input type="hidden" name="secure" value="'.$id.'"/>';
    }

}
